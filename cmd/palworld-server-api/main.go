package main

import (
	"bufio"
	"bytes"
	"context"
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"sync"
	"sync/atomic"
	"unicode"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

//go:embed index.html
var indexHtml []byte

const LISTEN_ADDRESS string = ":8212"
const YABSNAP_CONF string = "/etc/yabsnap/configs/palworld-mikunia.conf"
const SYSTEMD_USER string = "podman"
const SYSTEMD_SERVICE string = "palworld-mikunia.service"
const SYSTEMD_SOCKET string = "palworld-mikunia-trigger.socket"
const RCON_EXE string = "rcon"
const RCON_ADDRESS string = "127.0.0.1:25575"

var (
	AuthKey        string
	ListenAddress  string = LISTEN_ADDRESS
	YabsnapConf    string = YABSNAP_CONF
	SystemdUser    string = SYSTEMD_USER
	SystemdService string = SYSTEMD_SERVICE
	SystemdSocket  string = SYSTEMD_SOCKET
	RconExe        string = RCON_EXE
	RconAddress    string = RCON_ADDRESS
	RconPassword   string
)

type Snapshot struct {
	Comment    string `json:"comment"`
	ConfigFile string `json:"config_file"`
	File       struct {
		Prefix    string `json:"prefix"`
		Timestamp string `json:"timestamp"`
	} `json:"file"`
	Source  string `json:"source"`
	Trigger string `json:"trigger"`
}

type Player struct {
	Name      string `json:"name"`
	PlayerUID string `json:"playeruid"`
	SteamID   string `json:"steamid"`
}

type RconInfo struct {
	Info        string    `json:"info"`
	ShowPlayers string    `json:"show_players"`
	Version     string    `json:"version"`
	Players     []*Player `json:"players"`
}

type MultiReaderWriter struct {
	buf    bytes.Buffer
	closed bool
	lock   sync.RWMutex
	cond   *sync.Cond
}

type BufferReader struct {
	rb  *MultiReaderWriter
	pos int
}

func NewMultiReaderWriter() *MultiReaderWriter {
	rb := &MultiReaderWriter{}
	rb.cond = sync.NewCond(&rb.lock)
	return rb
}

func (rb *MultiReaderWriter) NewReader() *BufferReader {
	return &BufferReader{rb: rb}
}

func (rb *MultiReaderWriter) Write(p []byte) (n int, err error) {
	rb.lock.Lock()
	defer rb.lock.Unlock()
	if rb.closed {
		return 0, io.EOF
	}
	if len(p) == 0 {
		return 0, nil
	}
	n, err = rb.buf.Write(p)
	rb.cond.Broadcast()
	return
}

func (rb *MultiReaderWriter) Close() error {
	rb.lock.Lock()
	defer rb.lock.Unlock()
	rb.closed = true
	rb.cond.Broadcast()
	return nil
}

func (rr *BufferReader) Read(p []byte) (n int, err error) {
	lock := rr.rb.lock.RLock
	unlock := rr.rb.lock.RUnlock
	var wait bool
	rem := len(p)
	for n == 0 {
		if wait {
			lock = rr.rb.lock.Lock
			unlock = rr.rb.lock.Unlock
		}
		lock()
		c := copy(p, rr.rb.buf.Bytes()[rr.pos:min(rr.pos+rem, rr.rb.buf.Len())])
		n += c
		rr.pos += c
		rem -= c
		if rem > 0 {
			if rr.rb.closed {
				err = io.EOF
				unlock()
				return
			}
			if n == 0 {
				if wait {
					rr.rb.cond.Wait()
				} else {
					wait = true
				}
			}
		}
		unlock()
	}
	return
}

func progRun(ctx context.Context, stdoutReader func(stdout io.Reader) error, stderrReader func(stderr io.Reader) error, args ...string) error {
	cmd := exec.CommandContext(ctx, args[0], args[1:]...)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return fmt.Errorf("[>] %s: %w", strings.Join(args, " "), err)
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		return fmt.Errorf("[>] %s: %w", strings.Join(args, " "), err)
	}
	err = cmd.Start()
	if err != nil {
		return fmt.Errorf("[>] %s: %w", strings.Join(args, " "), err)
	}
	in, out := io.Pipe()
	subErrsCh := make(chan error, 2)
	printErrCh := make(chan error)
	go func() {
		var b []byte
		b, _ = io.ReadAll(io.TeeReader(stderr, out))
		out.Close()
		b = bytes.TrimSpace(b)
		if len(b) == 0 {
			printErrCh <- nil
		} else {
			printErrCh <- fmt.Errorf("%s", bytes.ReplaceAll(b, []byte{'\n'}, []byte{' '}))
		}
	}()
	go func() {
		err := stderrReader(in)
		io.Copy(io.Discard, in)
		in.Close()
		subErrsCh <- err
	}()
	go func() {
		err := stdoutReader(stdout)
		io.Copy(io.Discard, stdout)
		subErrsCh <- err
	}()
	for i := 0; i < 2; i++ {
		subErr := <-subErrsCh
		if err != nil {
			err = fmt.Errorf("%w: %w", err, subErr)
		} else {
			err = subErr
		}
	}
	printErr := <-printErrCh
	waitErr := cmd.Wait()
	if waitErr != nil {
		if err != nil {
			if printErr != nil {
				err = fmt.Errorf("%w: %w", err, printErr)
			}
		} else {
			err = printErr
		}
		if err != nil {
			return fmt.Errorf("[>] %s (%d): %w: %w", strings.Join(args, " "), cmd.ProcessState.ExitCode(), waitErr, err)
		} else {
			return fmt.Errorf("[>] %s (%d): %w", strings.Join(args, " "), cmd.ProcessState.ExitCode(), waitErr)
		}
	}
	return err
}

func discarder(in io.Reader) error {
	io.Copy(io.Discard, in)
	return nil
}

func teef(out io.Writer) func(io.Reader) error {
	return func(r io.Reader) error {
		io.Copy(out, r)
		return nil
	}
}

func ParseYabsnapConfig(conf string) (map[string]string, error) {
	f, err := os.Open(conf)
	if err != nil {
		return nil, fmt.Errorf("'%s': %w", conf, err)
	}
	defer f.Close()
	props := make(map[string]string, 13)
	buff := bufio.NewScanner(f)
	for buff.Scan() {
		l := buff.Text()
		l = strings.TrimSpace(l)
		if len(l) == 0 || strings.HasPrefix(l, "#") {
			continue
		}
		ls := strings.SplitN(l, "=", 2)
		if len(ls) < 2 {
			continue
		}
		k := strings.TrimSpace(ls[0])
		v := strings.TrimSpace(ls[1])
		if len(k) == 0 || len(v) == 0 {
			continue
		}
		props[k] = v
	}
	return props, nil
}

func GetRconInfo(ctx context.Context) (*RconInfo, error) {
	var ri RconInfo
	args := []string{RconExe, "-a", RconAddress, "-p", RconPassword, "Info"}
	err := progRun(ctx, func(stdout io.Reader) error {
		b, _ := io.ReadAll(stdout)
		b = bytes.TrimSpace(b)
		if len(b) == 0 {
			return nil
		}
		ri.Info = string(b)
		rx := regexp.MustCompile(`v[0-9]\.[0-9]\.[0-9]\.[0-9]`)
		ri.Version = rx.FindString(ri.Info)
		return nil
	}, discarder, args...)
	if err != nil {
		return nil, err
	}
	args = []string{RconExe, "-a", RconAddress, "-p", RconPassword, "ShowPlayers"}
	err = progRun(ctx, func(stdout io.Reader) error {
		b, _ := io.ReadAll(stdout)
		b = bytes.TrimSpace(b)
		if len(b) == 0 {
			return nil
		}
		ri.ShowPlayers = string(b)
		bufs := bufio.NewScanner(strings.NewReader(ri.ShowPlayers))
		bufs.Scan() // skip header
		for bufs.Scan() {
			l := bufs.Text()
			l = strings.TrimSpace(l)
			if len(l) == 0 {
				continue
			}
			ls := strings.SplitN(l, ",", 3)
			if len(ls) < 3 {
				continue
			}
			ri.Players = append(ri.Players, &Player{
				Name:      strings.TrimSpace(ls[0]),
				PlayerUID: strings.TrimSpace(ls[1]),
				SteamID:   strings.TrimSpace(ls[2]),
			})
		}
		return nil
	}, discarder, args...)
	return &ri, err
}

func GetSystemdServiceInfo(ctx context.Context) (map[string]string, error) {
	props := make(map[string]string, 284)
	return props, progRun(ctx, func(stdout io.Reader) error {
		var err error
		r := bufio.NewReader(stdout)
		for err == nil {
			var b []byte
			b, err = r.ReadBytes('\n')
			b = bytes.TrimSpace(b)
			if len(b) == 0 {
				continue
			}
			splits := bytes.SplitN(b, []byte{'='}, 2)
			if len(splits) != 2 {
				continue
			}
			props[string(bytes.TrimSpace(splits[0]))] = string(bytes.TrimSpace(splits[1]))
		}
		return nil
	}, discarder, "systemctl", "-M", "podman@", "--user", "show", SystemdService)
}

func SystemdServiceStart(ctx context.Context) error {
	return progRun(ctx, discarder, discarder, "systemctl", "-M", "podman@", "--no-block", "--user", "start", SystemdService)
}

func SystemdServiceStop(ctx context.Context) error {
	return progRun(ctx, discarder, discarder, "systemctl", "-M", "podman@", "--no-block", "--user", "stop", SystemdService)
}

func SystemdServiceRestart(ctx context.Context) error {
	return progRun(ctx, discarder, discarder, "systemctl", "-M", "podman@", "--no-block", "--user", "restart", SystemdService)
}

func SystemReboot(ctx context.Context) error {
	return progRun(ctx, discarder, discarder, "reboot")
}

func GetSnapshots(ctx context.Context) ([]*Snapshot, error) {
	var snaps []*Snapshot
	return snaps, progRun(ctx, func(stdout io.Reader) error {
		var err error
		r := bufio.NewReader(stdout)
		for err == nil {
			var b []byte
			var s Snapshot
			b, err = r.ReadBytes('\n')
			b = bytes.TrimSpace(b)
			if len(b) == 0 ||
				json.Unmarshal(b, &s) != nil ||
				s.ConfigFile != YabsnapConf {
				continue
			}
			snaps = append(snaps, &s)
		}
		return nil
	}, discarder, "yabsnap", "--config-file", YabsnapConf, "list-json")
}

var rollbackinprogress atomic.Bool

func Rollback(ctx context.Context, id string, w io.Writer, c chan error) error {
	if !rollbackinprogress.CompareAndSwap(false, true) {
		err := fmt.Errorf("rollback already in progress")
		if c != nil {
			c <- err
		}
		return err
	}
	if c != nil {
		c <- nil
	}
	defer rollbackinprogress.Store(false)
	if w == nil {
		w = io.Discard
	}
	print := func(args ...string) {
		fmt.Fprintf(w, "[$] %s\n", strings.Join(args, " "))
	}
	defer func() {
		args := []string{"nft", "destroy", "table", "inet", "palworld"}
		print(args...)
		progRun(ctx, discarder, discarder, args...)
	}()
	for _, f := range []func() error{func() error {
		args := []string{"nft", "destroy", "table", "inet", "palworld"}
		print(args...)
		return progRun(ctx, teef(w), discarder, args...)
	}, func() error {
		args := []string{"nft", "add", "table", "inet", "palworld"}
		print(args...)
		return progRun(ctx, teef(w), discarder, args...)
	}, func() error {
		args := []string{"nft", "add", "chain", "inet", "palworld", "input", "{ type filter hook input priority filter + 100; policy accept; }"}
		print(args...)
		return progRun(ctx, teef(w), discarder, args...)
	}, func() error {
		args := []string{"nft", "add", "rule", "inet", "palworld", "input", "udp", "dport", "{ 8211, 27015 }", "drop"}
		print(args...)
		return progRun(ctx, teef(w), discarder, args...)
	}, func() error {
		args := []string{"systemctl", "-M", "podman@", "--user", "stop", SystemdService}
		print(args...)
		return progRun(ctx, teef(w), discarder, args...)
	}, func() error {
		args := []string{"yabsnap", "--config-file", YabsnapConf, "create", "--comment", "rollback"}
		print(args...)
		return progRun(ctx, teef(w), discarder, args...)
	}, func() error {
		props, err := ParseYabsnapConfig(YabsnapConf)
		if err != nil {
			return fmt.Errorf("error parsing yabsnap config: %w", err)
		}
		ysSource, ok := props["source"]
		if !ok || len(ysSource) == 0 {
			return fmt.Errorf("missing 'source' property in yabsnap config")
		}
		ysDestPrefix, ok := props["dest_prefix"]
		if !ok || len(ysDestPrefix) == 0 {
			return fmt.Errorf("missing 'dest_prefix' property in yabsnap config")
		}
		ysDest := ysDestPrefix + id
		ysDestFi, err := os.Stat(ysDest)
		if err != nil {
			return fmt.Errorf("'%s': %w", ysDest, err)
		}
		if !ysDestFi.IsDir() {
			return fmt.Errorf("'%s': not a directory", ysDest)
		}
		args := []string{"find", ysSource, "-mindepth", "1", "-maxdepth", "1", "-exec", "rm", "-rf", "{}", "+"}
		print(args...)
		err = progRun(ctx, teef(w), discarder, args...)
		if err != nil {
			return err
		}
		args = []string{"cp", "-a", ysDest + "/.", ysSource + "/"}
		print(args...)
		err = progRun(ctx, teef(w), discarder, args...)
		if err != nil {
			return err
		}
		return nil
	}} {
		if err := f(); err != nil {
			return err
		}
	}
	return nil
}

func FirewallBlock(ctx context.Context) error {
	for _, f := range []func() error{func() error {
		args := []string{"nft", "destroy", "table", "inet", "palworld"}
		return progRun(ctx, discarder, discarder, args...)
	}, func() error {
		args := []string{"nft", "add", "table", "inet", "palworld"}
		return progRun(ctx, discarder, discarder, args...)
	}, func() error {
		args := []string{"nft", "add", "chain", "inet", "palworld", "input", "{ type filter hook input priority filter + 100; policy accept; }"}
		return progRun(ctx, discarder, discarder, args...)
	}, func() error {
		args := []string{"nft", "add", "rule", "inet", "palworld", "input", "udp", "dport", "{ 8211, 27015 }", "drop"}
		return progRun(ctx, discarder, discarder, args...)
	}} {
		if err := f(); err != nil {
			return err
		}
	}
	return nil
}

func FirewallAllow(ctx context.Context) error {
	args := []string{"nft", "destroy", "table", "inet", "palworld"}
	return progRun(ctx, discarder, discarder, args...)
}

func FirewallActive(ctx context.Context) (bool, error) {
	args := []string{"nft", "list", "table", "inet", "palworld"}
	err := progRun(ctx, discarder, discarder, args...)
	if err == nil {
		return true, nil
	}
	var exitError *exec.ExitError
	if !errors.As(err, &exitError) {
		return false, err
	}
	switch exitError.ExitCode() {
	case 0:
		return true, nil
	case 1:
		return false, nil
	default:
		return false, err
	}
}

func DownloadCurrent(ctx context.Context, w io.Writer) error {
	props, err := ParseYabsnapConfig(YabsnapConf)
	if err != nil {
		return fmt.Errorf("error parsing yabsnap config: %w", err)
	}
	ysSource, ok := props["source"]
	if !ok || len(ysSource) == 0 {
		return fmt.Errorf("missing 'source' property in yabsnap config")
	}
	args := []string{"tar", "-cf", "-", "--numeric-owner", "-I", "zstd -19", "-C", ysSource, "."}
	return progRun(ctx, teef(w), discarder, args...)
}

func DownloadSnapshot(ctx context.Context, id string, w io.Writer) error {
	props, err := ParseYabsnapConfig(YabsnapConf)
	if err != nil {
		return fmt.Errorf("error parsing yabsnap config: %w", err)
	}
	ysDestPrefix, ok := props["dest_prefix"]
	if !ok || len(ysDestPrefix) == 0 {
		return fmt.Errorf("missing 'dest_prefix' property in yabsnap config")
	}
	ysDest := ysDestPrefix + id
	ysDestFi, err := os.Stat(ysDest)
	if err != nil {
		return fmt.Errorf("'%s': %w", ysDest, err)
	}
	if !ysDestFi.IsDir() {
		return fmt.Errorf("'%s': not a directory", ysDest)
	}
	args := []string{"tar", "-cf", "-", "--numeric-owner", "-I", "zstd -19", "-C", ysDest, "."}
	return progRun(ctx, teef(w), discarder, args...)
}

func main() {
	if authKey, hasAuthKey := os.LookupEnv("AUTH_KEY"); !hasAuthKey || len(authKey) == 0 {
		log.Fatalln("AUTH_KEY environment variable is empty.")
	} else {
		AuthKey = authKey
		os.Unsetenv("AUTH_KEY")
	}
	if rconPassword, hasRconPassword := os.LookupEnv("RCON_PASSWORD"); !hasRconPassword || len(rconPassword) == 0 {
		log.Fatalln("RCON_PASSWORD environment variable is empty.")
	} else {
		RconPassword = rconPassword
		os.Unsetenv("RCON_PASSWORD")
	}
	if listenAddress, hasListenAddress := os.LookupEnv("LISTEN_ADDRESS"); hasListenAddress && len(listenAddress) > 0 {
		ListenAddress = listenAddress
	}
	if yabsnapConf, hasYabsnapConf := os.LookupEnv("YABSNAP_CONF"); hasYabsnapConf && len(yabsnapConf) > 0 {
		YabsnapConf = yabsnapConf
	}
	if systemdUser, hasSystemdUser := os.LookupEnv("SYSTEMD_USER"); hasSystemdUser && len(systemdUser) > 0 {
		SystemdUser = systemdUser
	}
	if systemdService, hasSystemdService := os.LookupEnv("SYSTEMD_SERVICE"); hasSystemdService && len(systemdService) > 0 {
		SystemdService = systemdService
	}
	if systemdSocket, hasSystemdSocket := os.LookupEnv("SYSTEMD_SOCKET"); hasSystemdSocket && len(systemdSocket) > 0 {
		SystemdSocket = systemdSocket
	}
	if rconExe, hasRconExe := os.LookupEnv("RCON_EXE"); hasRconExe && len(rconExe) > 0 {
		RconExe = rconExe
	}
	if rconAddress, hasRconAddress := os.LookupEnv("RCON_ADDRESS"); hasRconAddress && len(rconAddress) > 0 {
		RconAddress = rconAddress
	}
	e := echo.New()
	e.Use(
		middleware.Decompress(),
		middleware.Gzip(),
		middleware.RateLimiter(middleware.NewRateLimiterMemoryStore(10)))
	e.GET("/", func(c echo.Context) error {
		return c.HTMLBlob(http.StatusOK, indexHtml)
	}, middleware.CSRF())
	api := e.Group("/api")
	api.Use(
		middleware.CORS(),
		middleware.KeyAuth(func(key string, c echo.Context) (bool, error) {
			return key == AuthKey, nil
		}))
	api.GET("/check", func(c echo.Context) error {
		return c.JSON(http.StatusOK, struct {
			Message string `json:"message"`
		}{Message: "success"})
	})
	api.GET("/info", func(c echo.Context) error {
		cherr := make(chan error, 2)
		chri := make(chan *RconInfo, 1)
		chsi := make(chan map[string]string, 1)
		go func() {
			ri, err := GetRconInfo(c.Request().Context())
			cherr <- err
			chri <- ri
		}()
		go func() {
			props, err := GetSystemdServiceInfo(c.Request().Context())
			cherr <- err
			chsi <- props
		}()
		var err error
		for i := 0; i < 2; i++ {
			e := <-cherr
			if e != nil {
				if err != nil {
					err = fmt.Errorf("%w;%w", err, e)
				} else {
					err = e
				}
			}
		}
		var mess string
		if err != nil {
			mess = err.Error()
		}
		return c.JSON(http.StatusOK, struct {
			Message        string            `json:"message"`
			RconInfo       *RconInfo         `json:"rcon_info"`
			SystemdService map[string]string `json:"systemd_service"`
		}{Message: mess, RconInfo: <-chri, SystemdService: <-chsi})
	})
	api.GET("/snapshots", func(c echo.Context) error {
		snaps, err := GetSnapshots(c.Request().Context())
		if err != nil {
			return c.JSON(http.StatusInternalServerError, struct {
				Message string `json:"message"`
			}{Message: err.Error()})
		}
		return c.JSON(http.StatusOK, struct {
			Snapshots []*Snapshot `json:"snapshots"`
		}{
			Snapshots: snaps,
		})
	})
	var rbinprogress atomic.Bool
	var rbmrwMutex sync.Mutex
	var rbmrw *MultiReaderWriter
	var rbwg sync.WaitGroup
	rbctx := context.Background()
	api.GET("/rollback", func(c echo.Context) error {
		rbmrwMutex.Lock()
		if rbmrw == nil {
			rbmrwMutex.Unlock()
			return c.JSON(http.StatusNoContent, struct {
				Message string `json:"message"`
			}{Message: "success"})
		}
		r := rbmrw.NewReader()
		rbmrwMutex.Unlock()
		c.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		c.Response().WriteHeader(http.StatusOK)
		enc := json.NewEncoder(c.Response())
		br := bufio.NewScanner(r)
		for br.Scan() {
			enc.Encode(struct {
				Message string `json:"message"`
			}{Message: strings.TrimRightFunc(br.Text(), unicode.IsSpace)})
			c.Response().Flush()
		}
		return nil
	})
	api.POST("/rollback/:id", func(c echo.Context) error {
		if !rbinprogress.CompareAndSwap(false, true) {
			return c.JSON(http.StatusBadRequest, struct {
				Message string `json:"message"`
			}{Message: fmt.Errorf("rollback already in progress").Error()})
		}
		id := c.Param("id")
		if len(id) == 0 {
			return c.JSON(http.StatusBadRequest, struct {
				Message string `json:"message"`
			}{Message: fmt.Errorf("missing parameter snapshot id").Error()})
		}
		snaps, err := GetSnapshots(c.Request().Context())
		if err != nil {
			return c.JSON(http.StatusInternalServerError, struct {
				Message string `json:"message"`
			}{Message: err.Error()})
		}
		var found bool
		for _, s := range snaps {
			if s.File.Timestamp == id {
				found = true
				break
			}
		}
		if !found {
			return c.JSON(http.StatusBadRequest, struct {
				Message string `json:"message"`
			}{Message: "unknown snapshot id"})
		}
		ce := make(chan error)
		rbwg.Add(1)
		go func() {
			defer rbwg.Done()
			defer rbinprogress.Store(false)
			rbmrwMutex.Lock()
			if rbmrw != nil {
				rbmrw.Close()
			}
			rbmrw = NewMultiReaderWriter()
			rbmrwMutex.Unlock()
			err := Rollback(rbctx, id, rbmrw, ce)
			if err != nil {
				fmt.Fprintln(rbmrw, err)
			}
			rbmrw.Close()
		}()
		err = <-ce
		if err != nil {
			return c.JSON(http.StatusInternalServerError, struct {
				Message string `json:"message"`
			}{Message: err.Error()})
		}
		return c.JSON(http.StatusOK, struct {
			Message string `json:"message"`
		}{Message: "success"})
	})
	api.POST("/start", func(c echo.Context) error {
		err := SystemdServiceStart(c.Request().Context())
		if err != nil {
			return c.JSON(http.StatusInternalServerError, struct {
				Message string `json:"message"`
			}{Message: err.Error()})
		}
		return c.JSON(http.StatusOK, struct {
			Message string `json:"message"`
		}{Message: "success"})
	})
	api.POST("/stop", func(c echo.Context) error {
		err := SystemdServiceStop(c.Request().Context())
		if err != nil {
			return c.JSON(http.StatusInternalServerError, struct {
				Message string `json:"message"`
			}{Message: err.Error()})
		}
		return c.JSON(http.StatusOK, struct {
			Message string `json:"message"`
		}{Message: "success"})
	})
	api.POST("/restart", func(c echo.Context) error {
		err := SystemdServiceRestart(c.Request().Context())
		if err != nil {
			return c.JSON(http.StatusInternalServerError, struct {
				Message string `json:"message"`
			}{Message: err.Error()})
		}
		return c.JSON(http.StatusOK, struct {
			Message string `json:"message"`
		}{Message: "success"})
	})
	api.POST("/reboot", func(c echo.Context) error {
		err := SystemReboot(c.Request().Context())
		if err != nil {
			return c.JSON(http.StatusInternalServerError, struct {
				Message string `json:"message"`
			}{Message: err.Error()})
		}
		return c.JSON(http.StatusOK, struct {
			Message string `json:"message"`
		}{Message: "success"})
	})
	api.POST("/firewallblock", func(c echo.Context) error {
		err := FirewallBlock(c.Request().Context())
		if err != nil {
			return c.JSON(http.StatusInternalServerError, struct {
				Message string `json:"message"`
			}{Message: err.Error()})
		}
		return c.JSON(http.StatusOK, struct {
			Message string `json:"message"`
		}{Message: "success"})
	})
	api.POST("/firewallallow", func(c echo.Context) error {
		err := FirewallAllow(c.Request().Context())
		if err != nil {
			return c.JSON(http.StatusInternalServerError, struct {
				Message string `json:"message"`
			}{Message: err.Error()})
		}
		return c.JSON(http.StatusOK, struct {
			Message string `json:"message"`
		}{Message: "success"})
	})
	api.GET("/firewallactive", func(c echo.Context) error {
		state, err := FirewallActive(c.Request().Context())
		if err != nil {
			return c.JSON(http.StatusInternalServerError, struct {
				Message string `json:"message"`
			}{Message: err.Error()})
		}
		return c.JSON(http.StatusOK, struct {
			State bool `json:"state"`
		}{State: state})
	})
	gka := middleware.DefaultKeyAuthConfig
	gka.KeyLookup = "query:key"
	gka.Validator = func(key string, c echo.Context) (bool, error) {
		return key == AuthKey, nil
	}
	e.GET("/download", func(c echo.Context) error {
		c.Response().Header().Set(echo.HeaderContentType, "application/x-gtar")
		c.Response().Header().Set(echo.HeaderContentDisposition, `attachment; filename="snapshot-current.tar.zst"`)
		c.Response().WriteHeader(http.StatusOK)
		return DownloadCurrent(c.Request().Context(), c.Response())
	}, middleware.KeyAuthWithConfig(gka))
	e.GET("/download/:id", func(c echo.Context) error {
		id := c.Param("id")
		c.Response().Header().Set(echo.HeaderContentType, "application/x-gtar")
		c.Response().Header().Set(echo.HeaderContentDisposition, fmt.Sprintf(`attachment; filename="snapshot-%s.tar.zst"`, id))
		c.Response().WriteHeader(http.StatusOK)
		return DownloadSnapshot(c.Request().Context(), id, c.Response())
	}, middleware.KeyAuthWithConfig(gka))
	e.Start(ListenAddress)
	rbwg.Wait()
}
